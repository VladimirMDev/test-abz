import { useInfiniteQuery } from '@tanstack/react-query'
import { Container } from '../shared/Container/Container'
import { Button } from '../shared/Button/Button'
import { UserCard } from './components/Card/UserCard.jsx'
import { Title } from '../shared/Title/Title'
import { Preloader } from '../shared/Preloader/Preloader'
import { fetchUsers } from '../../api/utils/fetchUsers.js'
import classes from './UserList.module.scss'

export const UserList = ({ type, styles = {}, refForScroll }) => {
  const { data, error, status, hasNextPage, fetchNextPage } = useInfiniteQuery({
    queryFn: fetchUsers,
    initialPageParam: 1,
    queryKey: ['users'],
    getNextPageParam: (lastPage) => {
      return lastPage.nextPageNumber ?? undefined
    },
  })

  return (
    <section ref={refForScroll} style={styles} className={classes['user-list']}>
      <Container type={type}>
        <Title text={'Working with GET request'} />
        {status === 'pending' ? (
          <Preloader styles={{ marginTop: '100px', height: '530px' }} />
        ) : status === 'error' ? (
          <p>{error}</p>
        ) : (
          <>
            <div className={classes['content']}>
              {data.pages.map(({ users }) => {
                return users.map((user) => {
                  const { id, photo, name, position, email, phone } = user
                  return (
                    <UserCard
                      key={id}
                      photo={photo}
                      name={name}
                      position={position}
                      email={email}
                      phone={phone}
                    />
                  )
                })
              })}
            </div>
            {hasNextPage && (
              <Button
                callback={fetchNextPage}
                text={'Show more'}
                styles={{ minWidth: '120px' }}
              />
            )}
          </>
        )}
      </Container>
    </section>
  )
}
