import { Tooltip } from 'react-tooltip'
import { prepareName } from '../../helpers/prepareName.js'
import { clsx } from 'clsx'
import placeholderPhoto from '../../../../assets/user-list-section/placeholder-photo.svg'
import classes from './UserCard.module.scss'

export const UserCard = ({ photo, name, position, email, phone, id }) => {
  const nameForShow = prepareName(name)

  return (
    <article className={classes.card} key={id}>
      <Tooltip id="user-card-tooltip" noArrow={true} offset={18} />
      <div className={classes['img-wrapper']}>
        {photo ? (
          <img
            className={classes.photo}
            src={photo}
            alt={name}
            draggable={false}
            onError={(e) => {
              e.target.src = placeholderPhoto
            }}
          />
        ) : (
          <img
            className={classes.photo}
            src={placeholderPhoto}
            alt={name}
            draggable={false}
          />
        )}
      </div>
      <span className={clsx(classes.ellipsis, classes.name)}>
        {nameForShow}
      </span>
      <span className={classes.ellipsis}>{position}</span>
      <span
        className={classes.ellipsis}
        data-tooltip-id="user-card-tooltip"
        data-tooltip-content={email}
        data-tooltip-place="bottom"
      >
        {email}
      </span>
      <span className={classes.ellipsis}>{phone}</span>
    </article>
  )
}
