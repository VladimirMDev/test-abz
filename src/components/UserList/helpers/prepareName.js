export const prepareName = (name) => {
  if (!name.length) return ''

  function capitalizeFirstLetter(str) {
    const firstLetter = str.slice(0, 1).toUpperCase()
    return firstLetter + str.slice(1, str.length)
  }

  const splitedName = name.split(' ')

  const nameWithCapitalLetter = splitedName
    .map((text) => capitalizeFirstLetter(text))
    .join(' ')

  const NAME_LENGTH_LIMIT = 20
  return nameWithCapitalLetter.length > 18
    ? nameWithCapitalLetter.slice(0, NAME_LENGTH_LIMIT) + '...'
    : nameWithCapitalLetter
}
