import classes from './Button.module.scss'

export const Button = ({
  type = 'button',
  text,
  disabled = false,
  callback,
  styles,
}) => {
  return (
    <button
      type={type}
      onClick={callback}
      disabled={disabled}
      className={classes.button}
      style={styles}
    >
      {text}
    </button>
  )
}
