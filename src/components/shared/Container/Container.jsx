import { clsx } from 'clsx'
import classes from './Container.module.scss'

export const Container = ({ type, children, outPaddings }) => {
  const getClass = (deviceType) => {
    if (deviceType === 'pc') return 'container-pc'
    if (deviceType === 'laptop') return 'container-laptop'
    if (deviceType === 'tablet') return 'container-tablet'
    if (deviceType === 'mobile') return 'container-mobile'
    console.error(`Unknown device type ${deviceType}`)
    return 'mobile'
  }

  return (
    <div className={classes['out-paddings']}>
      {outPaddings}
      <div className={clsx(classes.container, classes[getClass(type)])}>
        {children}
      </div>
    </div>
  )
}
