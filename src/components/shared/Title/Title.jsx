import classes from './Title.module.scss'

export const Title = ({ text, styles = {} }) => {
  return (
    <h2 style={styles} className={classes.title}>
      {text}
    </h2>
  )
}
