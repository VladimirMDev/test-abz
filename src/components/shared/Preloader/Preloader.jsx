import classes from './Preloader.module.scss'
import loaderImg from './../../../assets/preloader.svg'

export const Preloader = ({ styles }) => {
  return (
    <div style={styles}>
      <img
        className={classes['loader-img']}
        src={loaderImg}
        alt="loader"
        draggable={false}
      />
    </div>
  )
}
