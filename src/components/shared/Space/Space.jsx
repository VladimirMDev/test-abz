export const Space = ({ height, children }) => {
  return <div style={{ marginTop: `${height}px` }}>{children}</div>
}
