export const calculateOptimalVariant = (width) => {
  if (width === 360) return '360'
  if (width > 580 && width <= 768) return '768'
  if (width > 768 && width <= 1024) return '1024'
  if (width > 1024) return '1170'
  return 'default'
}
