const imagesOnServer = {
  360: `https://lh4.googleusercontent.com/l9Ouhmba_581xc1TevYhv7TOlKa8MiXZycadSXuYWKC7rhylW8Arf0HztIkG8dAOnwWh6bDWpjlSc1g2srC2-E1aKj6McXZ_zof258K6aadn9POpPvy866Gb_7j7a6yrVA=w1280`,
  768: `https://lh5.googleusercontent.com/i97X5SndPkogGgYa7Cg4nZxx0ACyjrKdkayHDpMMWScwWDK_zjdmv4msZz7IGCcOhATOKf7dEiE4AVanhGoVEdaOma56v-3R8k6b_Akf77mjM-z9ObAsJ83S6sepN6ZSYg=w1280`,
  1024: `https://lh3.googleusercontent.com/q8S6Ex04g6Oyp2MiM7moF9ptWRdNZmIW7rXfxvxtEuGx3F6CijV2cRNJ0SPNs5K9fOI1JjEQuiTZaRnLAPvGjwOAeDIqsH6gv5z3TmQMw4ru3URsPX8kaA4vWXN0hIKf=w1280`,
  1170: `https://lh3.googleusercontent.com/89St9MWGWz46hLaQ4AheUMFc63HBQbFxNq1PvO7F-P0XkU_waVslPG5YRGjDsNSsKLQcS0QUIBG7GRrhQJnPgGPEcR2KHuG3JlwFiIAmQ7uxSqaAV8Yqih49Ww7HS0A3AQ=w1280`,
  default: `https://lh3.googleusercontent.com/89St9MWGWz46hLaQ4AheUMFc63HBQbFxNq1PvO7F-P0XkU_waVslPG5YRGjDsNSsKLQcS0QUIBG7GRrhQJnPgGPEcR2KHuG3JlwFiIAmQ7uxSqaAV8Yqih49Ww7HS0A3AQ=w1280`,
}

export const imagePicker = (variant) => {
  if (imagesOnServer[variant]) {
    return imagesOnServer[variant]
  } else {
    console.warn(`Variant ${variant} not found. Set default image`)
    return imagesOnServer.default
  }
}
