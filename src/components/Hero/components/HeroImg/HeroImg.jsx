import defaultHeroImg from '../../../../assets/hero-section/hero-1170.jpg'
import { useEffect, useState } from 'react'
import { useUserScreenWidth } from '../../../../hooks/useUserScreenWidth.js'
import { calculateOptimalVariant } from './helpers/calculateOptimalVariant.js'
import { imagePicker } from './helpers/imagePicker.js'
import classes from './HeroImg.module.scss'

export const HeroImg = () => {
  const { screenWidth } = useUserScreenWidth()
  const [pathToImage, setPathToImage] = useState(null)

  useEffect(() => {
    const variant = calculateOptimalVariant(screenWidth)
    const imgPath = imagePicker(variant)
    setPathToImage(imgPath)
  }, [screenWidth])

  return (
    <img
      className={classes['hero-img']}
      src={pathToImage}
      alt="Field image"
      draggable={false}
      onError={(e) => {
        console.warn(
          `Link to image ${e.target?.src} is broken. Image set to default`,
        )
        e.target.src = defaultHeroImg
      }}
    />
  )
}
