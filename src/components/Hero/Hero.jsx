import { Button } from '../shared/Button/Button'
import { Container } from '../shared/Container/Container'
import { HeroImg } from './components/HeroImg/HeroImg'
import classes from './Hero.module.scss'

export const Hero = ({ type, scrollUtils }) => {
  const { scrollTo, ScrollTargets } = scrollUtils
  return (
    <section className={classes.hero}>
      <Container outPaddings={<HeroImg type={type} />} type={type}>
        <div className={classes['hero-content']}>
          <h1 className={classes.title}>
            Test assignment for front-end developer
          </h1>
          <p className={classes.text}>
            What defines a good front-end developer is one that has skilled
            knowledge of HTML, CSS, JS with a vast understanding of User design
            thinking as they&apos;ll be building web interfaces with
            accessibility in mind. They should also be excited to learn, as the
            world of Front-End Development keeps evolving.
          </p>
          <div>
            <Button
              callback={() => scrollTo(ScrollTargets.formRef)}
              text={'Sign up'}
            />
          </div>
        </div>
      </Container>
    </section>
  )
}
