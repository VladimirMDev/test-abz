import classes from './Logo.module.scss'
import logoImg from './../../../assets/logo.svg'

export const Logo = () => {
  return (
    <img className={classes.logo} src={logoImg} alt="logo" draggable={false} />
  )
}
