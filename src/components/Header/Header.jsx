import { Logo } from './Logo/Logo'
import { Button } from '../shared/Button/Button'
import { Container } from '../shared/Container/Container'
import classes from './Header.module.scss'

export const Header = ({ type, scrollUtils }) => {
  const { targets, scrollTo } = scrollUtils
  return (
    <header className={classes.header}>
      <Container type={type}>
        <div className={classes.inner}>
          <a href="#">
            <Logo />
          </a>
          <div className={classes.buttons}>
            <Button
              callback={() => scrollTo(targets.usersRef)}
              text={'Users'}
            />
            <Button
              callback={() => scrollTo(targets.formRef)}
              text={'Sign up'}
            />
          </div>
        </div>
      </Container>
    </header>
  )
}
