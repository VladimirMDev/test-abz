import { useEffect, useRef, useState } from 'react'
import { imageValidation } from '../helpers/validation/imageValidation/imageValidation.js'
import { imageValidationConfig } from '../helpers/validation/imageValidation/imageValidationConfig.js'

export const usePhotoValidation = () => {
  const [photo, setPhoto] = useState(null)
  const [photoErrors, setPhotoErrors] = useState('Photo is required field')
  const isFirstRender = useRef(true)

  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false
    } else {
      setPhotoErrors(null)
      imageValidation({
        img: photo,
        callback: setPhotoErrors,
        imageValidationConfig,
      })
    }
  }, [photo])

  return { photo, setPhoto, photoErrors }
}
