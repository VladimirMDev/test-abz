import { getUniqueId } from '../../../../helpers/getUniqueId.js'
import classes from './RadioButtonsSelect.module.scss'

export const RadioButtonsSelect = ({
  title,
  variants,
  fieldName,
  helperText,
  register,
}) => {
  return (
    <div className={classes.wrapper}>
      <span className={classes.title}>{title}</span>
      <fieldset className={classes.fieldset}>
        {variants.map((variant) => {
          const newId = `${variant.name}${getUniqueId()}`
          return (
            <div key={newId} className={classes['fieldset-row']}>
              <input
                className={classes.radio}
                name={fieldName}
                value={variant.id}
                type="radio"
                id={newId}
                {...register(fieldName)}
              />
              <label className={classes.label} htmlFor={newId}>
                {variant.name}
              </label>
            </div>
          )
        })}
        <span className={classes.helper}>{helperText}</span>
      </fieldset>
    </div>
  )
}
