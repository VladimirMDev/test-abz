import { Title } from '../../../shared/Title/Title.jsx'
import successImage from '../../../../assets/form-section/success-image.svg'

export const SuccessRegistration = () => {
  return (
    <>
      <Title text={'User successfully registered'} />
      <img src={successImage} alt="success" />
    </>
  )
}
