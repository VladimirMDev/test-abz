import { clsx } from 'clsx'
import { useId } from 'react'
import classes from './InputFileUploader.module.scss'

export const InputFileUploader = ({
  labelText,
  onChange,
  errorText,
  photo,
}) => {
  const id = useId()
  return (
    <>
      <div
        className={clsx(
          classes['input-file-uploader'],
          photo && errorText && classes.wrong,
        )}
      >
        <input
          id={id}
          className={clsx(classes.input)}
          onChange={onChange}
          accept="image/*"
          type="file"
        />
        <label
          className={clsx(classes.label, photo && errorText && classes.wrong)}
          htmlFor={id}
        >
          {labelText}
        </label>
      </div>
      {photo && <span className={classes.helper}>{errorText}</span>}
    </>
  )
}
