import { Input } from '../Input/Input.jsx'
import classes from '../../Form.module.scss'

export const Inputs = ({ fields, register, errorsOfValidation }) => {
  return (
    <div className={classes.inputs}>
      {!!fields.length &&
        fields.map((field) => {
          const { fieldName, type, placeholderText, autocomplete } = field
          return (
            <Input
              key={fieldName}
              type={type}
              placeholderText={placeholderText}
              errorMessage={errorsOfValidation[field.fieldName]?.message}
              register={register}
              fieldName={field.fieldName}
              autocomplete={autocomplete}
            />
          )
        })}
    </div>
  )
}
