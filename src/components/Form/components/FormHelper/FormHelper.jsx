import { DisappearingComponent } from '../../../../mocks/DisappearingComponent/DisappearingComponent.jsx'
import classes from './FormHelper.module.scss'

export const FormHelper = ({ errors, baseDelay = 4 }) => {
  const errorTexts = errors.split('\\n')
  return (
    <>
      {errorTexts.map((errorText, index) => {
        return (
          <DisappearingComponent
            key={errorText + index}
            delay={baseDelay + index}
          >
            <span className={classes['helper-text']}>{errorText}</span>
          </DisappearingComponent>
        )
      })}
    </>
  )
}
