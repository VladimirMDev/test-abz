import { Preloader } from '../../../shared/Preloader/Preloader.jsx'
import { RadioButtonsSelect } from '../RadioButtonsSelect/RadioButtonsSelect.jsx'
import classes from './PositionsSelect.module.scss'

export const PositionsSelect = ({
  errorsOfLoading,
  isLoading,
  positions,
  fieldName,
  register,
  title,
  errorsOfValidation,
}) => {
  return (
    <>
      {errorsOfLoading ? (
        <p className={classes['error-message']}>{errorsOfLoading}</p>
      ) : isLoading ? (
        <Preloader styles={{ marginBottom: '50px' }} />
      ) : (
        !!positions.length && (
          <>
            <RadioButtonsSelect
              title={title}
              register={register}
              fieldName={fieldName}
              variants={positions}
              helperText={errorsOfValidation[fieldName]?.message}
            />
          </>
        )
      )}
    </>
  )
}
