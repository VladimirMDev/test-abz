import { useState } from 'react'
import { clsx } from 'clsx'
import classes from './Input.module.scss'

export const Input = ({
  type = 'text',
  placeholderText,
  errorMessage,
  fieldName,
  register = () => {},
  autocomplete = 'off',
}) => {
  const [focus, setFocus] = useState(false)
  return (
    <div className={classes.wrapper}>
      <label className={classes.label}>
        <input
          className={clsx(classes.input, !!errorMessage && classes['error'])}
          placeholder={placeholderText ?? ''}
          type={type}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          {...register(fieldName)}
          autoComplete={autocomplete}
        />
      </label>
      <span
        className={clsx(
          classes.helper,
          focus ? classes.show : classes.hide,
          !!errorMessage && classes['error'],
        )}
      >
        {errorMessage}
      </span>
    </div>
  )
}
