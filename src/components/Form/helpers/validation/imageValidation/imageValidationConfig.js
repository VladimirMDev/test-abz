export const imageValidationConfig = {
  types: ['image/jpeg', 'image/jpg'],
  size: 5 * 1024 * 1024,
  width: 70,
  height: 70,
}
