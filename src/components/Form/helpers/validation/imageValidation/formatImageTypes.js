export function formatImageTypes(types) {
  const uniqueTypes = Array.from(new Set(types))
  const formattedTypes = uniqueTypes.map((type) => {
    const lowercaseType = type.toLowerCase()
    return lowercaseType.split('/')[1].toUpperCase()
  })
  return formattedTypes.join('/')
}