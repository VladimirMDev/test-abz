import { checkImageResolution } from './checkImageResolution.js'
import { formatBytesToReadableValue } from '../../../../../helpers/formatBytesToReadableValue.js'
import { formatImageTypes } from './formatImageTypes.js'

export const imageValidation = ({ img, callback, imageValidationConfig }) => {
  const {
    types,
    size,
    width: expectedWidth,
    height: expectedHeight,
  } = imageValidationConfig
  if (!img) {
    callback('Image is required field')
    return
  }
  if (types && !types.includes(img.type)) {
    callback(`The image format should be ${formatImageTypes(types)}`)
    return
  }
  if (size && img.size > size) {
    callback(
      `The image size must not exceed ${formatBytesToReadableValue(size)}`,
    )
    return
  }
  try {
    checkImageResolution({
      img,
      callback,
      expectedWidth,
      expectedHeight,
    })
  } catch (err) {
    callback(err.message)
  }
}
