export async function checkImageResolution({
  img,
  callback,
  expectedWidth,
  expectedHeight,
}) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = function () {
      const image = new Image()
      image.src = reader.result
      image.onload = function () {
        try {
          const { width: widthUserImage, height: heightUserImage } = image
          if (
            heightUserImage < expectedHeight ||
            widthUserImage < expectedWidth
          ) {
            callback(
              `You upload image ${widthUserImage}x${heightUserImage}. The image resolution at least ${expectedWidth}x${expectedHeight}px minimal.`,
            )
          }
          resolve()
        } catch (e) {
          reject(e)
        }
      }
    }
    reader.readAsDataURL(img)
  })
}
