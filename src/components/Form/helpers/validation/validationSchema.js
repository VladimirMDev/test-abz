import {
  phoneStartWith380,
  emailContainSymbolAtRegex,
  emailFullCheckRegex,
  phoneDoNotContainLettersRegex,
  phoneFullCheckRegex,
} from '../../../../constants/regex.js'
import * as yup from 'yup'

export const validationSchema = {
  name: yup
    .string()
    .required('Name is required field')
    .min(2, 'Name must be longer than 2 characters')
    .max(60, 'The name must be shorter than 60 characters'),
  email: yup
    .string()
    .required('Email is required field')
    .matches(emailContainSymbolAtRegex, 'email must contain @')
    .matches(emailFullCheckRegex, 'Example mail@mailprovider.com'),
  phone: yup
    .string()
    .required('Phone is required field')
    .matches(phoneStartWith380, 'Phone number, should start with +380')
    .matches(
      phoneDoNotContainLettersRegex,
      'The phone should not contain letters',
    )
    .matches(phoneFullCheckRegex, '+38 (XXX) XXX - XX - XX'),
  positions: yup.number().required('Positions is a required field'),
}
export const schema = yup.object(validationSchema).required()
