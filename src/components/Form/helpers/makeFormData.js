export const makeFormData = (data) => {
  const { name, email, phone, positions, photo } = data

  const formData = new FormData()
  formData.append('name', name)
  formData.append('email', email)
  formData.append('phone', phone)
  formData.append('position_id', positions)
  formData.append('photo', photo)
  return formData
}
