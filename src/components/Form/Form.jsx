import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { Button } from '../shared/Button/Button'
import { Title } from '../shared/Title/Title'
import { Container } from '../shared/Container/Container'
import { InputFileUploader } from './components/InputFileUploader/InputFileUploader.jsx'
import { PositionsSelect } from './components/PositionsSelect/PositionsSelect.jsx'
import { Inputs } from './components/Inputs/Inputs.jsx'
import { SuccessRegistration } from './components/SuccessRegistration/SuccessRegistration'
import { FormHelper } from './components/FormHelper/FormHelper'
import { useGetPositions } from '../../api/hooks/useGetPositions.js'
import { useSentForm } from '../../api/hooks/useSentForm.js'
import { usePhotoValidation } from './hooks/usePhotoValidation.js'
import { makeFormData } from './helpers/makeFormData.js'
import { schema } from './helpers/validation/validationSchema.js'
import { apiAbz } from '../../api/apiAbz.js'
import classes from './Form.module.scss'

const inputsConfig = [
  {
    type: 'text',
    placeholderText: 'Your name',
    fieldName: 'name',
    autocomplete: 'name',
  },
  {
    type: 'text',
    placeholderText: 'Email',
    fieldName: 'email',
    autocomplete: 'email',
  },
  {
    type: 'text',
    placeholderText: 'Phone',
    fieldName: 'phone',
    autocomplete: 'tel',
  },
]

export const Form = ({ type, styles = {}, refForScroll }) => {
  const { photo, setPhoto, photoErrors } = usePhotoValidation()
  const positionsController = useGetPositions(apiAbz)
  const formController = useSentForm(apiAbz)

  const {
    register,
    handleSubmit,
    formState: { errors: validationErrors, isValid },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
  })

  const submitFormHandler = (data, e) => {
    e.preventDefault()
    if (isValid && !photoErrors && photo && !formController.formIsSubmitting) {
      const mergePhotoAndInputsData = { ...data, ...photo }
      const formData = makeFormData(mergePhotoAndInputsData)
      formController.formSubmit(formData)
    }
  }

  return (
    <section
      className={classes['form-section']}
      ref={refForScroll}
      style={styles}
    >
      <Container type={type}>
        {formController.formHasBeenSubmit ? (
          <SuccessRegistration />
        ) : (
          <>
            <Title text={'Working with POST request'} />
            <form
              className={classes.form}
              onSubmit={handleSubmit(submitFormHandler)}
            >
              <div className={classes['form-content']}>
                <Inputs
                  fields={inputsConfig}
                  errorsOfValidation={validationErrors}
                  register={register}
                />
                <PositionsSelect
                  title={'Select your position'}
                  errorsOfLoading={positionsController.errors}
                  errorsOfValidation={validationErrors}
                  isLoading={positionsController.loading}
                  positions={positionsController.data}
                  register={register}
                  fieldName={'positions'}
                />
                <InputFileUploader
                  labelText={
                    photo?.name
                      ? `${photoErrors ? '❌' : '✔️'} ${photo.name}`
                      : 'Upload photo'
                  }
                  errorText={photoErrors}
                  photo={photo}
                  onChange={(e) => {
                    setPhoto(e.target.files[0])
                  }}
                />
                {!!formController.errorsOfSubmitting && (
                  <FormHelper errors={formController.errorsOfSubmitting} />
                )}
              </div>
              <div className={classes.button}>
                <Button
                  type="submit"
                  disabled={!isValid || photoErrors}
                  text={'Sign up'}
                />
              </div>
            </form>
          </>
        )}
      </Container>
    </section>
  )
}
