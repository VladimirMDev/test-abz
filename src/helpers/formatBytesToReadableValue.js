export const formatBytesToReadableValue = (bytes) => {
  const KILOBYTE = 1024
  const MEGABYTE = KILOBYTE * 1024

  if (bytes < KILOBYTE) {
    return bytes + ' bytes'
  } else if (bytes < MEGABYTE) {
    return (bytes / KILOBYTE).toFixed(2) + ' KB'
  } else {
    return (bytes / MEGABYTE).toFixed(2) + ' MB'
  }
}
