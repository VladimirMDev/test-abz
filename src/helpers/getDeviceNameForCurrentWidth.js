const config = {
  breakpoints: [
    { width: 1170, deviceName: 'pc' },
    { width: 1024, deviceName: 'laptop' },
    { width: 768, deviceName: 'tablet' },
    { width: 360, deviceName: 'mobile' },
  ],
  defaultDeviceName: 'mobile',
}

export const getDeviceNameForCurrentWidth = (width) => {
  for (let i = 0; i < config.breakpoints.length; i++) {
    const breakpoint = config.breakpoints[i]
    if (i === config.breakpoints.length - 1) {
      return config.defaultDeviceName
    }
    if (width >= breakpoint.width) {
      return breakpoint.deviceName
    }
  }
}
