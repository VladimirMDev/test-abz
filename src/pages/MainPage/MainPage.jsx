import { useScrollTo } from '../../hooks/useScrollTo.js'
import { useUserScreenWidth } from '../../hooks/useUserScreenWidth.js'
import { getDeviceNameForCurrentWidth } from '../../helpers/getDeviceNameForCurrentWidth.js'
import { Header } from '../../components/Header/Header.jsx'
import { Hero } from '../../components/Hero/Hero.jsx'
import { UserList } from '../../components/UserList/UserList.jsx'
import { Form } from '../../components/Form/Form.jsx'

export const MainPage = () => {
  const scrollUtils = useScrollTo()
  const { screenWidth } = useUserScreenWidth()
  const device = getDeviceNameForCurrentWidth(screenWidth)

  return (
    <main>
      <Header type={device} scrollUtils={scrollUtils} />
      <Hero scrollUtils={scrollUtils} type={device} />
      <UserList
        type={device}
        refForScroll={scrollUtils.ScrollTargets.usersRef}
        styles={{ marginTop: '140px' }}
      />
      <Form
        type={device}
        refForScroll={scrollUtils.ScrollTargets.formRef}
        styles={{ marginTop: '140px', marginBottom: '100px' }}
      />
    </main>
  )
}
