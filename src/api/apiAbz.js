import { RequestController } from './requestController.js'

export const apiAbz = new RequestController(
  'https://frontend-test-assignment-api.abz.agency/api/v1',
)
