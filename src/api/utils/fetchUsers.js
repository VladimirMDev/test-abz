import { apiAbz } from '../apiAbz.js'

export const fetchUsers = async ({ pageParam }) => {
  try {
    const response = await apiAbz.getUsers(pageParam)
    const { users } = response
    const nextPageUrl = response.links.next_url
    const nextPageNumber = extractPageNumber(nextPageUrl)

    return { users, nextPageNumber }
  } catch (err) {
    console.error('FetchUsers error', err.message)
  }
}

function extractPageNumber(url) {
  const regex = /page=(\d+)/
  const match = url.match(regex)
  if (match && match[1]) {
    return parseInt(match[1])
  } else {
    return null
  }
}
