export const sentFormErrorHandler = (error) => {
  const statuses = {
    validateFails: 422,
  }

  const status = error.response.status
  const message =
    error.response.data.message ||
    error.message ||
    'Whoops, looks like something went wrong.'
  if (status === statuses.validateFails) {
    const fails = error.response.data?.fails ?? {}
    return `${message} ${concatFailsToOneString(fails)}`
  }
  return message
}

function concatFailsToOneString(failsObj) {
  let str = ''
  if (Object.keys(failsObj).length) {
    for (const key in failsObj) {
      str += `${key}: ${failsObj[key][0]} \\n`
    }
  }
  return str
}
