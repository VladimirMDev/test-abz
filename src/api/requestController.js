import axios from 'axios'

export class RequestController {
  constructor(baseURL) {
    this.axiosInstance = axios.create({
      baseURL,
    })
  }

  getToken() {
    return this.makeRequest({
      url: `/token`,
      method: 'get',
    })
  }

  getPositions() {
    return this.makeRequest({
      url: `/positions`,
      method: 'get',
    })
  }

  getUsers(page) {
    const usersCountInEachResponse = 6
    const url = `/users?page=${page}&count=${usersCountInEachResponse.toString()}`
    return this.makeRequest({
      url,
      method: 'get',
    })
  }

  sentForm(data, token) {
    return this.makeRequest({
      url: `/users`,
      method: 'post',
      data,
      headers: {
        Token: token,
      },
    })
  }

  async makeRequest(requestConfig) {
    const { data } = await this.axiosInstance(requestConfig)
    return data
  }
}
