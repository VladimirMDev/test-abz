import { useCallback, useEffect, useState } from 'react'

export const useGetPositions = (api) => {
  const [positions, setPositions] = useState([])
  const [loading, setLoading] = useState(true)
  const [errors, setErrors] = useState(null)

  const fetchPositions = useCallback(async () => {
    try {
      setErrors(false)
      setLoading(true)

      const response = await api.getPositions()
      const { positions } = response

      positions.length
        ? setPositions(positions)
        : setErrors(() => 'Positions not found in response')
    } catch (err) {
      const errorMessage = err.message
      setErrors(errorMessage)
      console.error(errorMessage)
    } finally {
      setLoading(false)
    }
  }, [api])

  useEffect(() => {
    fetchPositions()
  }, [fetchPositions])

  return {
    data: positions,
    loading,
    errors,
  }
}
