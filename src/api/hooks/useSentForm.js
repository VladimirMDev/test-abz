import { useCallback, useState } from 'react'
import { useQueryClient } from '@tanstack/react-query'
import { sentFormErrorHandler } from '../utils/sentFormErrorHandler.js'

export const useSentForm = (api) => {
  const [formIsSubmitting, setFormIsSubmitting] = useState(null)
  const [errorsOfSubmitting, setErrorsOfSubmitting] = useState(null)
  const [formHasBeenSubmit, setFormHasBeenSubmit] = useState(null)
  const queryClient = useQueryClient()

  const formSubmit = useCallback(
    async (form) => {
      try {
        setErrorsOfSubmitting(false)
        setFormIsSubmitting(true)

        const { token } = await api.getToken()
        if (!token) {
          setErrorsOfSubmitting('Token not found in response')
          return
        }

        const response = await api.sentForm(form, token)

        if (response.success) {
          setFormHasBeenSubmit(true)
          await queryClient.resetQueries({ queryKey: ['users'], exact: true })
        } else {
          setErrorsOfSubmitting(`Error of sent form ${response.message || ''}`)
        }
      } catch (err) {
        const errorMessage = sentFormErrorHandler(err)
        setErrorsOfSubmitting(errorMessage)
      } finally {
        setFormIsSubmitting(false)
      }
    },
    [queryClient, api],
  )

  return { formSubmit, formHasBeenSubmit, formIsSubmitting, errorsOfSubmitting }
}
