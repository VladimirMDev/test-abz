import './styles/index.scss'
import {MainPage} from './pages/MainPage/MainPage.jsx'
import {QueryClient, QueryClientProvider} from '@tanstack/react-query'

function App() {
    const queryClient = new QueryClient()
    return (
        <QueryClientProvider client={queryClient}>
            <MainPage/>
        </QueryClientProvider>
    )
}

export default App
