import { useRef } from 'react'

export const useScrollTo = () => {
  const usersRef = useRef(null)
  const formRef = useRef(null)
  const scrollTo = (target) => {
    target.current.scrollIntoView({ behavior: 'smooth' })
  }
  return {
    ScrollTargets: {
      usersRef,
      formRef,
    },
    scrollTo,
  }
}
