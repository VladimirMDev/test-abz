import { useState, useEffect } from 'react'

const getWidthOfScreen = () => {
  return window.innerWidth
}

export function useUserScreenWidth() {
  const [screenWidth, setScreenWidth] = useState(getWidthOfScreen())

  useEffect(() => {
    const updateDevice = () => {
      setScreenWidth(getWidthOfScreen())
    }

    window.addEventListener('resize', updateDevice)

    updateDevice()

    return () => window.removeEventListener('resize', updateDevice)
  }, [])
  return { screenWidth }
}
